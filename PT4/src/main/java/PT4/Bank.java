package PT4;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

public class Bank implements BancProc, Serializable{
	private HashMap<Person,ArrayList<Account>> data;

	public void addPerson(Person p) {
		assert(p!=null);
		int initialSize=data.size();
		data.put(p, new ArrayList<Account>());
		assert(data.size()==initialSize+1);
	}

	public void addAccount(String cnp, Account a) {
		assert(cnp != null);
		Person person = null;
		int initialArraySize = 0;
		for(Person p : data.keySet()) {
			if(p.getCNP().equals(cnp)) {
				person = p;
				initialArraySize = data.get(p).size();
				ArrayList<Account> toAdd = new ArrayList<Account>(data.get(p));
				toAdd.add(a);
				data.put(p, toAdd);
			}
		}
		assert(data.get(person).size() == initialArraySize + 1);
	}
	
	public void addMoney(String accountId,int moneyToAdd) {
		assert(moneyToAdd > 0);
		for(List<Account > accountList : data.values()) {
			for(Account acc : accountList) {
				if(acc.getID().equals(accountId)) {
					((Account) acc).setBalance(acc.getBalance() + moneyToAdd);
				}
			}
		}
		//assert();
	}
	
	public void withdrawMoney(String accountId,int moneyToWith) {
		assert(moneyToWith > 0);
		for(List<Account > accountList : data.values()) {
			for(Account acc : accountList) {
				if(acc.getID().equals(accountId)) {
					((Account) acc).setBalance(acc.getBalance() - moneyToWith);
				}
			}
		}
		//assert(Account acc.getBalance() >= 0);
	}
	
	public void editPerson(String cnp, String newName) {
	//	assert(newName != null);
		Person person = null;
		for(Person p: data.keySet()) {
			if(p.getCNP().equals(cnp)) {
				person = p;
				p.setName(newName);
			}
		}
	//	assert();
	}
	
	public void editAccount(String accID, int newBalance, double newInterestRate) {
		for(List<Account> accountList : data.values()) {
			for(Account acc: accountList) {
				if(acc.getID().equals(accID)) {
					((Account) acc).setBalance(newBalance);
					((SavingAccount) acc).setInterestRate(newInterestRate);
				}
			}
		}
	}

	public Bank() {
		data = new  HashMap<Person,ArrayList<Account>>();
	}
	
	public void saveInfo(String fileName)
	{
	      try {
	          FileOutputStream fileOut = new FileOutputStream(fileName);
	          ObjectOutputStream out = new ObjectOutputStream(fileOut);
	          out.writeObject(this);
	          out.close();
	          fileOut.close();
	          System.out.println("Data is saved!");
	       } catch (IOException i) {
	          i.printStackTrace();
	       }
	}

	public static Bank loadInfo(String fileName)
	{
	      Bank b = null;
	      try {
	         FileInputStream fileIn = new FileInputStream(fileName);
	         ObjectInputStream in = new ObjectInputStream(fileIn);
	         b = (Bank) in.readObject();
	         in.close();
	         fileIn.close();
	      }catch (FileNotFoundException i) {
		         System.out.println("File not found");
		         return null; 
		  }catch (IOException i) {
	         return null;
	      } catch (ClassNotFoundException c) {
	         System.out.println("Bank class not found");
	         return null;
	      }
	      System.out.println("Data is loaded!");
	      return b;
	}
	
	public String toString()
	{
		String s= "";
		for (Entry<Person,ArrayList<Account>> e : data.entrySet())
		{
			s+="Accounts for : "+e.getKey()+"\n";
			for (Account a: e.getValue())
				s+=a+"\n";	
			s+="\n";
		}
		return s;

	}

	public HashMap<Person, ArrayList<Account>> getData() {
		return data;
	}

	public void deleteAccount(String CNP, String accID) {
		for (Entry<Person,ArrayList<Account>> e : data.entrySet())
		{			
			if (e.getKey().getCNP().equals(CNP))
				for (Account a: e.getValue())
				{
					if (a.getID().equals(accID))
					{	
						data.get(e.getKey()).remove(a);
						return;
					}
				}
		}		
	}	
	
}

package PT4;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

public class GUI extends JFrame{
	
	private Bank b;
	
	private Vector<String> accountsTableColumns;
	private Vector<String> personsTableColumns;
	private JTable table;
	private JScrollPane jsTable;
	private JPanel tablePanel;
	private JPanel buttonPanel;
	
	//private JTable tablePers;
	
	
	public GUI(final Bank b)
	{
		this.b=b;
		
		accountsTableColumns = new Vector<String>();
		accountsTableColumns.add("CNP");
		accountsTableColumns.add("Name");
		accountsTableColumns.add("AccountID");
		accountsTableColumns.add("Balance");
		accountsTableColumns.add("AccountType");	
		accountsTableColumns.add("InterestRate");
		
		personsTableColumns = new Vector<String>();
		personsTableColumns.add("CNP");
		personsTableColumns.add("Name");
		
		setTitle("Bank");
		setLayout(new FlowLayout());
		
		tablePanel= new JPanel();
		jsTable=new JScrollPane(table);
		tablePanel.add(jsTable);
		
		buttonPanel = new JPanel();
		
		final JLabel addNameLabel = new JLabel("Name:");
		final JTextField addNameField = new JTextField();
		final JLabel addCnpLabel = new JLabel("CNP:");
		final JTextField addCnpField = new JTextField();
		addNameField.setColumns(12);
		addCnpField.setColumns(12);
		addNameLabel.setVisible(false);
		addNameField.setVisible(false);
		addCnpLabel.setVisible(false);
		addCnpField.setVisible(false); 
		
		
		final JLabel editNameLabel = new JLabel("New Name:");
		final JTextField editNameField = new JTextField();
		editNameField.setColumns(12);
		editNameLabel.setVisible(false);
		editNameField.setVisible(false);
		
		final JLabel addPersIdLabel = new JLabel("Person CNP:");
		final JTextField addPersIdField = new JTextField();
		addPersIdField.setColumns(12);
		final JLabel addIdAccLabel = new JLabel("Add ID:");
		final JTextField addIdAccField = new JTextField();
		addIdAccField.setColumns(12);
		final JLabel addCurrencyLabel = new JLabel("Add Currency");
		final JTextField addCurrencyField = new JTextField();
		addCurrencyField.setColumns(12);
		final JLabel addBalanceLabel = new JLabel("Add Balance:");
		final JTextField addBalanceField = new JTextField();
		addBalanceField.setColumns(12);
		final JLabel addInterestRateLabel = new JLabel("Add Interest Rate:");
		final JTextField addInterestRateField = new JTextField();
		addInterestRateField.setColumns(12);
		addPersIdLabel.setVisible(false);
		addPersIdField.setVisible(false);
		addIdAccLabel.setVisible(false);
		addIdAccField.setVisible(false);
		addCurrencyLabel.setVisible(false);
		addCurrencyField.setVisible(false);
		addBalanceLabel.setVisible(false);
		addBalanceField.setVisible(false);
		addInterestRateLabel.setVisible(false);
		addInterestRateField.setVisible(false);
		
		final JLabel addMoneyLabel = new JLabel("ADD:");
		final JTextField addMoneyField = new JTextField();
		addMoneyField.setColumns(12);
		addMoneyLabel.setVisible(false);
		addMoneyField.setVisible(false);
		
		final JLabel withdrawMoneyLabel = new JLabel("WITHDRAW:");
		final JTextField withdrawMoneyField = new JTextField();
		withdrawMoneyField.setColumns(12);
		withdrawMoneyLabel.setVisible(false);
		withdrawMoneyField.setVisible(false);
		
		final JLabel newBalanceLabel = new JLabel("New Balance:");
		final JTextField newBalanceField = new JTextField();
		newBalanceField.setColumns(12);
		final JLabel newInterestRateLabel = new JLabel("New Interest Rate:");
		final JTextField newInterestRateField = new JTextField();
		newInterestRateField.setColumns(12);
		newBalanceLabel.setVisible(false);
		newBalanceField.setVisible(false);
		newInterestRateLabel.setVisible(false);
		newInterestRateField.setVisible(false);
		
		
		JButton jViewPers = new JButton("View Persons");
		jViewPers.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				updatePersonTable();
			}
		});
		
		final JButton ok1Button = new JButton("OK");
		ok1Button.setVisible(false);
		ok1Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Person p = new Person(addCnpField.getText(), addNameField.getText());
				b.addPerson(p);
				updatePersonTable();
				
				addNameLabel.setVisible(false);
				addNameField.setVisible(false);
				addCnpLabel.setVisible(false);
				addCnpField.setVisible(false);	
				ok1Button.setVisible(false);
			}
		});
		
		JButton jNewPers = new JButton("Add Person");
		jNewPers.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				addNameLabel.setVisible(true);
				addNameField.setVisible(true);
				addCnpLabel.setVisible(true);
				addCnpField.setVisible(true);
				ok1Button.setVisible(true);	
			}	
		});
		
		//ok2Button will be used to EDIT PERSONS
		final JButton ok2Button = new JButton("OK");	
		ok2Button.setVisible(false);
		ok2Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int row = table.getSelectedRow();
				if (row==-1)
					return;
				
				String CNP = table.getModel().getValueAt(row, 0).toString();
				String newName = editNameField.getText();
				
				b.editPerson(CNP, newName);
				updatePersonTable();
				
				editNameLabel.setVisible(false);
				editNameField.setVisible(false);
				ok2Button.setVisible(false);	
			}		
		});
		
		JButton jEditPers = new JButton("Edit Person");
		jEditPers.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				editNameLabel.setVisible(true);
				editNameField.setVisible(true); 
				ok2Button.setVisible(true);
				
			}
		});
		
		
		JButton jViewAcc = new JButton("View Accounts");
		jViewAcc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateAccountsTable();
			}			
		});
		
		final JButton okAddAccButt = new JButton("OK");
		okAddAccButt.setVisible(false);
		okAddAccButt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				double interestRate = Double.parseDouble(addInterestRateField.getText());
				Account a = null;
				if(interestRate == 0) {
					a = new SpendingAccount(addIdAccField.getText(), addCurrencyField.getText(), Integer.parseInt(addBalanceField.getText()));
				} else {
					a = new SavingAccount(addIdAccField.getText(), addCurrencyField.getText(), Integer.parseInt(addBalanceField.getText()), interestRate);
				}
				
				b.addAccount(addPersIdField.getText(), a);
				updateAccountsTable();
				
				addPersIdLabel.setVisible(false);
				addPersIdField.setVisible(false);
				addIdAccLabel.setVisible(false);
				addIdAccField.setVisible(false);
				addCurrencyLabel.setVisible(false);
				addCurrencyField.setVisible(false);
				addBalanceLabel.setVisible(false);
				addBalanceField.setVisible(false);
				addInterestRateLabel.setVisible(false);
				addInterestRateField.setVisible(false);
				okAddAccButt.setVisible(false);
			}	
		});
		
		JButton jAddAcc = new JButton("Add Account");
		jAddAcc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				addPersIdLabel.setVisible(true);
				addPersIdField.setVisible(true);
				addIdAccLabel.setVisible(true);
				addIdAccField.setVisible(true);
				addCurrencyLabel.setVisible(true);
				addCurrencyField.setVisible(true);
				addBalanceLabel.setVisible(true);
				addBalanceField.setVisible(true);
				addInterestRateLabel.setVisible(true);
				addInterestRateField.setVisible(true);
				okAddAccButt.setVisible(true);
			}		
		});
		
		final JButton okEditAccButt = new JButton("OK");
		okEditAccButt.setVisible(false);
		okEditAccButt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int row = table.getSelectedRow();
				if (row==-1)
					return;
				
				String accID = table.getModel().getValueAt(row, 2).toString();
				int newBalance = Integer.parseInt(newBalanceField.getText());
				double newInterestRate = Double.parseDouble(newInterestRateField.getText());
				
				b.editAccount(accID, newBalance, newInterestRate);
				updateAccountsTable();
				
				newBalanceLabel.setVisible(false);
				newBalanceField.setVisible(false);
				newInterestRateLabel.setVisible(false);
				newInterestRateField.setVisible(false);
				okEditAccButt.setVisible(false);
			}	
		});
		
		JButton jEditAcc = new JButton("Edit Account");
		jEditAcc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				newBalanceLabel.setVisible(true);
				newBalanceField.setVisible(true);
				newInterestRateLabel.setVisible(true);
				newInterestRateField.setVisible(true);
				okEditAccButt.setVisible(true);
			}		
		});
		
		JButton jDelAcc = new JButton("Delete Account");
		jDelAcc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int row = table.getSelectedRow();
				if (row==-1)
					return;
				String CNP = table.getModel().getValueAt(row, 0).toString();
				String accID = table.getModel().getValueAt(row, 2).toString();
				b.deleteAccount(CNP, accID);
				updateAccountsTable();
			}
		});
		
		final JButton okAddMonButt = new JButton("OK");
		okAddMonButt.setVisible(false);
		okAddMonButt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				int row = table.getSelectedRow();
				if (row==-1)
					return;
				String accID = table.getModel().getValueAt(row, 2).toString();
				int moneyToAdd = Integer.parseInt(addMoneyField.getText());
				
				b.addMoney(accID, moneyToAdd); 
				updateAccountsTable();
				
				addMoneyLabel.setVisible(false);
				addMoneyField.setVisible(false);
				okAddMonButt.setVisible(false);
			}
		});
		
		JButton addMoneyButt = new JButton("Add Money");
		addMoneyButt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				addMoneyLabel.setVisible(true);
				addMoneyField.setVisible(true);
				okAddMonButt.setVisible(true);
			}
		});
		
		final JButton okWithdrawMonButt = new JButton("OK");
		okWithdrawMonButt.setVisible(false);
		okWithdrawMonButt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int row = table.getSelectedRow();
				if (row==-1)
					return;
				
				String accID = table.getModel().getValueAt(row, 2).toString();
				int moneyToWith = Integer.parseInt(withdrawMoneyField.getText());
				
				b.withdrawMoney(accID, moneyToWith);
				updateAccountsTable();
				
				withdrawMoneyLabel.setVisible(false);
				withdrawMoneyField.setVisible(false);
				okWithdrawMonButt.setVisible(false);
				
			}		
		});
		
		JButton withdrawMoneyButt = new JButton("Withdraw Money");
		withdrawMoneyButt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				withdrawMoneyLabel.setVisible(true);
				withdrawMoneyField.setVisible(true);
				okWithdrawMonButt.setVisible(true);
			}
		});
		
		buttonPanel.add(jViewPers);
		
		buttonPanel.add(jNewPers);
		buttonPanel.add(addCnpLabel);
		buttonPanel.add(addCnpField);
		buttonPanel.add(addNameLabel);
		buttonPanel.add(addNameField);
		buttonPanel.add(ok1Button);
		
		buttonPanel.add(jEditPers);
		buttonPanel.add(editNameLabel);
		buttonPanel.add(editNameField);
		buttonPanel.add(ok2Button);
		
		buttonPanel.add(jViewAcc);
		
		buttonPanel.add(jAddAcc);
		buttonPanel.add(addPersIdLabel);
		buttonPanel.add(addPersIdField);
		buttonPanel.add(addIdAccLabel);
		buttonPanel.add(addIdAccField);
		buttonPanel.add(addCurrencyLabel);
		buttonPanel.add(addCurrencyField);
		buttonPanel.add(addBalanceLabel);
		buttonPanel.add(addBalanceField);
		buttonPanel.add(addInterestRateLabel);
		buttonPanel.add(addInterestRateField);
		buttonPanel.add(okAddAccButt);
		
		buttonPanel.add(jEditAcc);
		buttonPanel.add(newBalanceLabel);
		buttonPanel.add(newBalanceField);
		buttonPanel.add(newInterestRateLabel);
		buttonPanel.add(newInterestRateField);
		buttonPanel.add(okEditAccButt);
		
		buttonPanel.add(jDelAcc);
		
		buttonPanel.add(addMoneyButt);
		buttonPanel.add(addMoneyLabel);
		buttonPanel.add(addMoneyField);
		buttonPanel.add(okAddMonButt);
		
		
		buttonPanel.add(withdrawMoneyButt);
		buttonPanel.add(withdrawMoneyLabel);
		buttonPanel.add(withdrawMoneyField);
		buttonPanel.add(okWithdrawMonButt);
		
		add(buttonPanel);
		add(tablePanel);
		
		setSize(900, 900);
		pack();
		setVisible(true);
		
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e)
			{
				b.saveInfo("bank.ser");
			}
		});
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	
	public void updatePersonTable() {
		Vector<Vector<String>> k = new Vector<Vector<String>>();
		for (Entry<Person,ArrayList<Account>> e : b.getData().entrySet()) {
				Vector<String> v = new Vector<String>();
				v.add(e.getKey().getCNP());
				v.add(e.getKey().getName());
				k.add(v);
		}
		
		table = new JTable(k,personsTableColumns);
		
		jsTable=new JScrollPane(table);
		tablePanel.removeAll();
		tablePanel.add(jsTable);
		
		pack();
	}
	
	
	public void updateAccountsTable()
	{
		Vector<Vector<String>> t = new Vector<Vector<String>>();
		for (Entry<Person,ArrayList<Account>> e : b.getData().entrySet())
		{			
			for (Account a: e.getValue())
			{
				Vector<String> v = new Vector<String>();
				v.add(e.getKey().getCNP());
				v.add(e.getKey().getName());
				v.add(a.getID());
				v.add(a.getBalance()+"");
				
				if (a instanceof SavingAccount)
					v.add("Saving");
				else
					v.add("Spending");
				
				if (a instanceof SavingAccount)
					v.add(String.valueOf(((SavingAccount) a).getInterestRate()));
				else
					v.add("-");
				
				t.add(v);	   
			}
		}
		
		table = new JTable(t,accountsTableColumns);

		jsTable=new JScrollPane(table);
		tablePanel.removeAll();
		tablePanel.add(jsTable);
		
		pack();
	}
	
	
}


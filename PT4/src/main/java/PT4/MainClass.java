package PT4;


public class MainClass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Bank b = Bank.loadInfo("bank.ser");
		if (b==null)
		{
			b = new Bank();
			b.saveInfo("bank.ser");
		}
		else
			System.out.println(b);
		
		
		GUI g = new GUI(b);
		g.updateAccountsTable();
	}

}

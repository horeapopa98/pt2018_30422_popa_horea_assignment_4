package PT4;


public class SavingAccount extends Account{
	private double interestRate;

	public SavingAccount(String iD, String currency, int balance, double interestRate) {
		super(iD, currency, balance);
		this.interestRate = interestRate;
	}
	
	public double getInterestRate() {
		return interestRate;
	}
	
	public void setInterestRate(double newInterestRate) {
		this.interestRate = newInterestRate;
	}

	public String toString()
	{
		return super.toString()+ " "+ interestRate;
	}
}

package PT4;

import java.io.Serializable;

public class Person implements Serializable {
	private String CNP;
	private String name;
	
	public Person(String cnp, String name) {
		super();
		CNP = cnp;
		this.name = name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getCNP() {
		return CNP;
	}

	public String getName() {
		return name;
	}


	public String toString()
	{
		return name;
	}
}


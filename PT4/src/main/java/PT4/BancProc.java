package PT4;


public interface BancProc {
	
	/**
	 * Precondition: p!=null
	 * Postcondition: finalMapSize==initialMapSize+1
	 * @param p
	 */
	public void addPerson(Person p);
	
	/**
	 * Precondition: cnp must have a value different from null
	 * Postcondition: finalArraySize(p) = initialArraySize(p) + 1
	 * @param cnp
	 * @param a
	 */
	public void addAccount(String cnp, Account a);
	
	
	public void deleteAccount(String CNP, String accID);
	
	public void addMoney(String accountId,int moneyToAdd);
	
	public void withdrawMoney(String accountId,int moneyToWith);
	
	public void editPerson(String cnp, String newName);
	
	public void editAccount(String accID, int newBalance, double newInterestRate);
	
}

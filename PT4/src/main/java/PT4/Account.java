package PT4;

import java.io.Serializable;

public abstract class Account implements Serializable{
	private String ID;
	private String currency;
	private int balance;
	
	public Account(String iD, String currency, int balance) {
		super();
		ID = iD;
		this.currency = currency;
		this.balance = balance;
	}
	
	public String toString()
	{
		return ID+" "+currency+" "+balance;
	}

	public String getID() {
		return ID;
	}

	public String getCurrency() {
		return currency;
	}

	public int getBalance() {
		return balance;
	}
	
	public void setID(String newId) {
		this.ID = newId;
	}
	
	public void setCurrency(String newCurrency) {
		this.currency = newCurrency;
	}
	
	public void setBalance(int newBalance) {
		this.balance = newBalance;
	}
}
